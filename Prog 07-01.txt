#include <LiquidCrystal_I2C.h>
#include <Wire.h>

#define RELE_DL1 9       // HABILITA DL1 INVERSOR 1ª VELOCIDADE DE GIRO
#define RELE_DL2 10      // HABILITA DL2 INVERSOR 2ª VELOCIDADE DE GIRO
#define RELE_DL3 11      // HABILITA DL3 INVERSOR 3ª VELOCIDADE DE GIRO
#define RELE_PRATO 12    // HABILITA PISTÃO PRATO 
#define RELE_EXTRACAO 13 // HABILITA PISTÃO EXTRAÇÃO

#define BOTAO_VERDE 7     // ACIONA COMANDO PARA DL1 INVERSOR 1ª VELOCIDADE DE GIRO
#define BOTAO_PRETO 6     // ACIONA COMANDO DL3 INVERSOR EMERGENCIA
#define BOTAO_AZUL 5     // ACIONA COMANDO PISTÃO PRATO
#define BOTAO_VERMELHO 4  // ACIONA COMANDO PISTÃO EXTRAÇÃO


bool estavelA;    // Guarda o último estado estável do botão;
bool instavelA;   // Guarda o último estado instável do botão;
bool estavelB;    // Guarda o último estado estável do botão;
bool instavelB;   // Guarda o último estado instável do botão;
bool estavelC;    // Guarda o último estado estável do botão;
bool instavelC;   // Guarda o último estado instável do botão;
bool estavelD;    // Guarda o último estado estável do botão;
bool instavelD;   // Guarda o último estado instável do botão;
unsigned long bounce_timer;
bool estadoReleA;
bool estadoReleB;
bool estadoReleC;
bool estadoReleD;

#define counter_pin 8
LiquidCrystal_I2C lcd(0x27,16,2);
bool item_detected = false;
int item_counter = 0;



void setup() 
  {
  pinMode(RELE_DL2, OUTPUT);
  digitalWrite(RELE_DL2,HIGH);
  
  pinMode(BOTAO_AZUL, INPUT_PULLUP); 
  digitalWrite(RELE_PRATO, HIGH);
  pinMode(RELE_PRATO, OUTPUT); 
  estavelA = digitalRead(BOTAO_AZUL);
  estadoReleA;

  pinMode (BOTAO_VERMELHO, INPUT_PULLUP);
  digitalWrite(RELE_EXTRACAO, HIGH);
  pinMode (RELE_EXTRACAO, OUTPUT);
  estavelB = digitalRead(BOTAO_VERMELHO);
  estadoReleB;

  pinMode (BOTAO_PRETO, INPUT_PULLUP);
  digitalWrite(RELE_DL1, HIGH);
  pinMode (RELE_DL1, OUTPUT);
  estavelC = digitalRead(BOTAO_PRETO);
  estadoReleC;

  pinMode (BOTAO_VERDE, INPUT_PULLUP);
  digitalWrite(RELE_DL1, HIGH);
  pinMode (RELE_DL1, OUTPUT);
  estavelD = digitalRead(BOTAO_VERDE);
  estadoReleD;

 // Serial.begin(9600);
  pinMode(counter_pin , INPUT); 
  lcd.init(); 


  }

bool changed1() 
    {
     bool now1 = digitalRead(BOTAO_AZUL);
        {
         if (instavelA != now1) 
          {       
           bounce_timer = millis();   
           instavelA = now1;          
          }
          
         else if (millis() - bounce_timer > 10) 
               {  
                if (estavelA != now1) 
                 {           
                  estavelA = now1;                  
                  return 1;
                 }
               }
        }
     return 0;
    }

bool changed2() 
   {
    bool now2 = digitalRead(BOTAO_VERMELHO);
       {
        if (instavelB != now2) 
         {       
          bounce_timer = millis();   
          instavelB = now2; 
         }
                    
        else if (millis() - bounce_timer >= 10) 
              {  
               if (estavelB != now2) 
                {           
                 estavelB = now2;                  
                 return 1;
                }
              }
       } 
    return 0;    
    }

bool changed3() 
   {
    bool now3 = digitalRead(BOTAO_PRETO);
       {
        if (instavelC != now3) 
         {       
          bounce_timer = millis();   
          instavelC = now3;            
         }
           
        else if (millis() - bounce_timer > 10) 
              {  
               if (estavelC != now3) 
                {           
                 estavelC = now3;                  
                 return 1;
                }
              }
       } 
    return 0;    
    }

bool changed4() 
    {
     bool now4 = digitalRead(BOTAO_VERDE);
        {
         if (instavelD != now4) 
          {       
           bounce_timer = millis();   
           instavelD = now4;            
          }
          
         else if (millis() - bounce_timer > 10) 
               {  
                if (estavelD != now4) 
                 {           
                  estavelD = now4;                  
                  return 1;
                 }
               }
       } 
    return 0;    
    }

void updateCounter()
   {
  //  Serial.print("Contador : ");
 //   Serial.println(item_counter);
    
  //  lcd.clear();
    lcd.setBacklight(HIGH);
    lcd.setCursor(0,0);
    lcd.print("Contador:");
    lcd.setCursor(0,1);
    lcd.print(item_counter);




 // lcd.setCursor(0,0);
 // lcd.print("Hello, world!");
 // lcd.setCursor(0,1);
//  lcd.print("Ready... :)");
 // delay(1000);
   }

void loop() 
   {
    int val = digitalRead( counter_pin );

    if((item_detected == false) && ( val == 0 ))
      {
        item_detected = true;
        item_counter++;
        updateCounter();

      }

     else if((item_detected == true) && ( val == 1 ))
           {
            item_detected = false;
           }

     if (item_counter == 6)
      {
       digitalWrite (RELE_DL2, LOW); 
      }
  
     else if (item_counter == 75)
           {
            digitalWrite (RELE_DL2, HIGH);
           }

     if (item_counter == 77)
      {
       digitalWrite (RELE_DL1, HIGH);
       item_counter = 78;
      }
  
     if (digitalRead(BOTAO_VERDE) & (item_counter == 78))
      {
       digitalWrite (RELE_DL1, LOW);
      }

     if (item_counter == 79)
      {
       digitalWrite (RELE_DL1, HIGH);
       item_counter = 80;
      }

     if (digitalRead(BOTAO_VERDE) & (item_counter == 80))
      {
       digitalWrite (RELE_DL1, LOW);
      }

     if (item_counter == 81)
      {
       digitalWrite (RELE_DL1, HIGH);
       item_counter = 82;
      }

     if (digitalRead(BOTAO_VERDE) & (item_counter == 82))
      {
       digitalWrite (RELE_DL1, LOW);
      }

     if (item_counter == 83)
      {
       digitalWrite (RELE_DL1, HIGH);
       item_counter = 84;
      }

     if (digitalRead(BOTAO_VERDE) & (item_counter == 84))
      {
       digitalWrite (RELE_DL1, LOW);
      }

     if (item_counter == 85)
      {
       digitalWrite (RELE_DL1, HIGH);
       item_counter = 1;
      }
        
   
     if (changed1()) 
      { 
       if (digitalRead(BOTAO_AZUL))
        {   
         estadoReleA=!estadoReleA; 
        }
         digitalWrite(RELE_PRATO,estadoReleA);
      }
      
     if (changed2()) 
      { 
       if (digitalRead(BOTAO_VERMELHO))
        {   
         estadoReleB=!estadoReleB;
        }
         digitalWrite(RELE_EXTRACAO,estadoReleB);
      }
       
    if (changed3()) 
     { 
      if (digitalRead(BOTAO_PRETO))
       {   
        estadoReleC=!estadoReleC; 
       }
        digitalWrite(RELE_DL1,estadoReleC);
     }
          
     if (changed4()) 
     { 
      if (digitalRead(BOTAO_VERDE))
       {   
        estadoReleD=!estadoReleD; 
       }
        digitalWrite(RELE_DL1,estadoReleD);
     }
  }
